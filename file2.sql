select t.kazarma_name,
       ltrim(kazarma_name) pOch,
       rtrim(kazarma_name) pOung,
       trim(kazarma_name) pO2,
       upper(kazarma_name) kataH,
       lower(kazarma_name) kickH,
       initcap(kazarma_name) BHK,
       length(kazarma_name) Uz,
       lpad(kazarma_name,'7','*') BRXU,
       rpad(kazarma_name,'7','*') CHRXU,
       instr (kazarma_name,'V','1') nechinchida,
       substr(kazarma_name,3,4) qirqish,
       replace(kazarma_name,'V','x') al
       
from KAZARMA t;