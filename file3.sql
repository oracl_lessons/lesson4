create table fudbol_klub(
	fudbolchi_id int not null primary key,
	name varchar(2)
	club_tashkil_qilingan_yil date);

insert into fudbol_klub(
	fudbolchi_id,name,club_tashkil_qilingan_yil)values(
	12,'PX',to_date('1956.12.10','YYYY-MM-DD'));



create table fudbolchilar (fudbolchi_name varchar2(20),
       fudbolchi_id int,
       foreign key (fudbolchi_id) references fudbol_klub(fudbolchi_id))
;

insert into fudbolchilar(fudbolchi_name,fudbolchi_id)values(
       'asadulla',12);

select fudbolchi_name,
       initcap(fudbolchi_name) N,
       instr(fudbolchi_name,'u','2') I,
       substr(trim(fudbolchi_name),1,instr(trim(fudbolchi_name),' ','1')-1) ism,
       substr(trim(fudbolchi_name),instr(trim(fudbolchi_name),' ','1')+1)familya
 from FUDBOLCHILAR t